package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
   public int[][] buildPyramid(List<Integer> inputNumbers) {

        if(!pyramidValidChecker(inputNumbers)){
            throw new CannotBuildPyramidException();
        }
        List<Integer> listForPyramid = new ArrayList<>(inputNumbers);
        listForPyramid.sort(Comparator.naturalOrder());
        int rows = 0;
        int columns = -1;
        int counterForCheck = 0;

        while (counterForCheck != listForPyramid.size()){
            rows++;
            counterForCheck += rows;
            columns+=2;

        }
        int[][] pyramidArray2 = new int[rows][columns];
        int slide = 0;
        int indexForList = listForPyramid.size() - 1;
        for (int i = rows - 1; i >=0; i--){
            for (int j = columns - 1 - slide; j >= slide; j= j - 2){
                pyramidArray2[i][j] = listForPyramid.get(indexForList);
                indexForList--;
            }
            slide++;
        }

        return pyramidArray2;

    }

    private boolean pyramidValidChecker(List<Integer> inputNumbers){
        int counterForCheck = 0;
        int incr = 0;
        if (inputNumbers.size() > 2147450880)//Максимальный размер списка для котрого можно построить пирамиду в диапазоне до Integer.MAX_VALUE
        {
            return false;
        }
        for (Integer i:inputNumbers){
            if(i == null){
                return false;
            }
        }

        while (counterForCheck != inputNumbers.size()){
            incr++;
            counterForCheck += incr;
            if(counterForCheck > inputNumbers.size()){
                return false;
            }

        }
        return true;
    }

}