package com.tsystems.javaschool.tasks.calculator;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
        public String evaluate(String statement) {
        String resultString;
        if(!validCheck(statement)){
          return null;
        }
        else {
            String[] splStatement = splitStatement(statement);
            Stack<String> stack = new Stack<>();
            List<String> list = new ArrayList<>();
            for(String s1:splStatement){
                if(s1.toCharArray()[0]>= '0' && s1.toCharArray()[0] <= '9' ){
                    list.add(s1);
                }
                else {
                    if(s1.equals(")")){
                        String s2 = stack.peek();
                        while (!s2.equals("(")){
                            list.add(stack.pop());
                            s2 = stack.peek();
                        }
                        stack.pop();
                    }
                    else if(stack.empty() || s1.equals("(") || getPriority(s1) > getPriority(stack.peek())){
                        stack.push(s1);}
                    else{
                        list.add(stack.pop());
                        stack.push(s1);
                    }

                }
            }
            while (!stack.empty()){
                list.add(stack.pop());
            }

            List<String> revNot = new ArrayList<>();
            for (int i = 0; i < list.size(); i ++){
                if((list.get(i).toCharArray()[0]>= '0' && list.get(i).toCharArray()[0] <= '9')){
                    revNot.add(list.get(i));
                }
                else {
                    String result = calculate(list.get(i), revNot.get(revNot.size() - 2),  revNot.get(revNot.size() - 1));
                    if (result.equals("error")) {return null;}
                    revNot.remove(revNot.size() - 1);
                    revNot.remove(revNot.size() - 1);
                    revNot.add(result);
                }
            }
            resultString = revNot.get(0);
            DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
            decimalFormatSymbols.setDecimalSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("#.####", decimalFormatSymbols);
            resultString = decimalFormat.format(Double.parseDouble(resultString));
            return resultString;
        }

    }

    private boolean validCheck(String statement){
        try {
            char prev = 0;
            int parenthesesCounter = 0;
            if (statement.isEmpty()) {
                return false;
            }
            for (char ch : statement.toCharArray()) {
                if (!(ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '.' || ch == '('
                        || ch == ')' || (ch >= '0' && ch <= '9'))) {
                    return false;
                }
                if ( (prev == '.' && !(ch >= '0')||((prev == '+' || prev == '-' || prev == '*' ||
                        prev == '/') && !((ch >= '0')||(ch == '(')))) ||(prev ==')' && !(ch == '+' ||
                        ch == '-' || ch == '*' || ch == '/'|| ch>='0'))){
                    return false;
                }
                if (ch == '(') {
                    parenthesesCounter++;
                }
                if (ch == ')') {
                    parenthesesCounter--;
                }
                if (parenthesesCounter < 0) {
                    return false;
                }
                prev = ch;
            }
            if (parenthesesCounter != 0) {
                return false;
            } else {
                return true;
            }
        }
        catch (Exception e){
            return false;
        }
    }


    private int getPriority(String s) {
        if (s.equals("(") || s.equals(")")){
            return 0;
        }
        else if (s.equals("+")) {
            return 1;
        } else if (s.equals("-")) {
            return 1;
        } else if (s.equals("/")) {
            return 2;
        } else {
            return 2;
        }
    }

    private String calculate(String sign, String a, String b){
        switch (sign){
            case "+":
                return Double.toString((Double.parseDouble(a) + Double.parseDouble(b)));
            case "-":
                return Double.toString((Double.parseDouble(a) - Double.parseDouble(b)));
            case "*":
                return Double.toString((Double.parseDouble(a) * Double.parseDouble(b)));
            case "/":
                if (Double.parseDouble(b) == 0){return "error";}
                return Double.toString((Double.parseDouble(a) / Double.parseDouble(b)));
        }
        return "error";
    }

    private String[] splitStatement(String statement){
        statement = statement.replaceAll("\\(", "s(s" );
        statement = statement.replaceAll("\\)", "s)s" );
        statement = statement.replaceAll("\\*", "s*s" );
        statement = statement.replaceAll("/", "s/s" );
        statement = statement.replaceAll("\\+", "s+s" );
        statement = statement.replaceAll("-", "s-s" );
        statement = statement.replaceAll("ss", "s" );

        StringBuilder stringBuilder = new StringBuilder(statement);
        if(statement.toCharArray()[0] == 's'){
            stringBuilder = stringBuilder.deleteCharAt(0);
            statement = stringBuilder.toString();
        }
        else if(statement.toCharArray()[statement.length() - 1] == 's'){
            stringBuilder = stringBuilder.deleteCharAt(statement.length() - 1);
            statement = stringBuilder.toString();
        }
        return statement.split("s");
    }
}